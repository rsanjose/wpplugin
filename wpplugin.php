<?php
defined('ABSPATH') or die("Bye bye");

/*
Plugin Name: Plugin de pruebas
Plugin URI: https://www.rsanjoseo.com
Description: Prueba siguiendo https://raiolanetworks.es/blog/crear-plugin-wordpress/
Version: 1.0
Author: rSanjoSEO
Author URI: https://www.rsanjoseo.com
License: Commercial
*/

/**

Ejemplo de código a ejecutar al activar el plugin:
register_activation_hook(string  $archivo, callable  $función )

function ejemplo_activar()
{
//A partir de aquí escribe todas las tareas que quieres realizar en la activación
//Vas a añadir una función nueva. La sintaxis de add_option es la siguiente:add_option($nombre,$valor,'',$cargaautomatica)
add_option('mi_opcion',255,'','yes');
}
register_activation_hook(__FILE__,'ejemplo_activar');

Ejemplo de código a ejecutar al desactivar el plugin:
register_deactivation_hook( string $file, callable $function )

function ejemplo_desctivar()
{
//A partir de aqui escribe todas las tareas que quieres realizar en la desactivación
}
register_activation_hook(__FILE__,'ejemplo_desactivar');

Ejemplo de opción para agregar una opción de menú...
add_menu_page(string $nombre_pagina, string $nombre_menu, string $permisos, string $menu_slug, callable $funcion, string $url_icono, int $posicion)

 */

define('RAI_RUTA',plugin_dir_path(__FILE__));
define('RAI_NOMBRE', 'Mi menú');

include(RAI_RUTA . 'includes/funciones.php');
include(RAI_RUTA . 'includes/opciones.php');
