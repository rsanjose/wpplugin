<?php
defined('ABSPATH') or die("Bye bye");

// Top level menu del plugin
function rai_menu_administrador()
{
    add_menu_page(RAI_NOMBRE,RAI_NOMBRE,'manage_options',RAI_RUTA . '/admin/configuracion.php');
    add_submenu_page(RAI_RUTA . '/admin/raiola.php','Ejemplo submenu','Ejemplo submenu','manage_options',RAI_RUTA . '/admin/ejemplo-submenu.php');
}

// El hook admin_menu ejecuta la funcion rai_menu_administrador
add_action( 'admin_menu', 'rai_menu_administrador' );
