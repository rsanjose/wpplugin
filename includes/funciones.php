<?php
defined('ABSPATH') or die("Bye bye");

function rai_nuevos_botones($botones)
{
    $botones[] = 'fontselect';
    $botones[] = 'fontsizeselect';
    $botones[] = 'underline';
    return $botones;
}
add_filter( 'mce_buttons_3','rai_nuevos_botones');  // mce_buttons_3 sería la 3ª fila de botones del editor MCE

